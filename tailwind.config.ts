import type { Config } from "tailwindcss";

export default {
  content: [
    "./src/posts/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        "smoores-violet": {
          tint2: "#6e5bc4",
          tint1: "#503ab5",
          DEFAULT: "#3216b0",
          shade1: "#261088",
          shade2: "#1b0a6b",
        },
        "smoores-orange": {
          tint3: "#fffae6",
          tint2: "#ffe164",
          tint1: "#ffd939",
          DEFAULT: "#ffce00",
          shade1: "#c59f00",
          shade2: "#9b7d00",
        },
        foreground: "#373d3f",
        primary: "#373d3f",
      },
      width: {
        page: "40rem",
      },
    },
  },
  plugins: [],
} satisfies Config;

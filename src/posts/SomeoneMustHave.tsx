import { BlockQuote } from "@/components/BlockQuote";
import { DateLine } from "@/components/DateLine";
import { FigCaption } from "@/components/FigCaption";
import { Figure } from "@/components/Figure";
import { LeadIn } from "@/components/LeadIn";
import { Link } from "@/components/Link";
import { Paragraph } from "@/components/Paragraph";
import { UnderlinedHeading } from "@/components/UnderlinedHeading";
import { Metadata } from "@/metadata";

export const metadata: Metadata = {
  title: "“Someone must have figured this out…”",
  description:
    "Learning to make wind chimes is, it turns out, sort of like learning to make almost everything else.",
  date: "Jan. 19, 2025",
  slug: "someone_must_have",
};

export function SomeoneMustHave() {
  return (
    <article>
      <UnderlinedHeading>{metadata.title}</UnderlinedHeading>
      <DateLine date={metadata.date} />
      <section id="intro">
        <BlockQuote>
          <Paragraph>
            <LeadIn>
              Why are the lower-pitched chimes both longer <em>and</em> wider?
              Do the width and length both affect the pitch?
            </LeadIn>
          </Paragraph>
        </BlockQuote>
        <Paragraph>
          As usual, my sister-in-law, Taylor, was asking some excellent
          questions. We had just finished a thrown pottery class together, along
          with my wife and her other sister, and were standing on the front lawn
          admiring a very large, very beautifully tuned set of wind chimes. The
          chimes themselves were long and wide, about 2 inches in diameter and
          ranging from something like 18 to 36 inches in length. As much a
          visual statement as an aural one.
        </Paragraph>
        <Paragraph>
          Why <em>were</em> these lower-pitched chimes both longer and wider
          than the higher-pitched set we’d seen the previous day? We’d been on a
          wind chime kick during this trip — Taylor was on the lookout for a set
          of wind chimes after taking a class on conducting sound baths, and my
          wife and I had our own set at home that I was enamored with.
        </Paragraph>
        <Paragraph>
          My best guess was that diameter had more do to with the amplitude than
          the pitch. Each chime in an individual set seemed to be the same
          diameter, so it was certainly possible to adjust the pitch by only
          adjusting the length. Maybe only the length affected the pitch, but
          since people are better at hearing higher pitches than lower pitches,
          you also need to make the chimes wider, or you wouldn’t be able to
          hear the lower tones? It was kind of bothering me that I wasn’t sure.
          But something else occurred to me as we turned away to rejoin the rest
          of the group. These were just metal tubes, the kind that hardware
          stores sold in ten-foot lengths.
        </Paragraph>
        <BlockQuote>
          <Paragraph>
            Taylor… I think we could make these. There must be a formula to
            calculate the pitch based on the length of the tube.{" "}
            <em>Someone must have figured this out.</em>
          </Paragraph>
        </BlockQuote>
      </section>
      <section id="research">
        <Paragraph>
          <LeadIn>Quite a lot</LeadIn> of my most interesting projects have
          started with a similar sentiment. Often it turns out that I’m right —
          someone has figured out the fundamental challenge, and packaged the
          solution to make it easier for others to utilize. Finding those
          solutions is one of my favorite experiences. The world is full of
          people who’ve become obsessed with a single problem, learning its ins
          and outs, and who’ve decided to share their discoveries. I’m regularly
          astonished by how often those people are willing to share their
          discoveries for nothing in return.
        </Paragraph>
        <Paragraph>
          And I had a hunch that this was another one of those times. Wind
          chimes are fascinating, both aesthetically and mechanically, and
          humans have been creating them for thousands of years. So I started
          searching around the Internet for information about the formulas for
          chime pitch, and I stumbled into what has since become{" "}
          <Link href="https://leehite.org/Chimes.htm">
            one of my favorite websites
          </Link>
          .
        </Paragraph>
        <Figure>
          {/* eslint-disable-next-line @next/next/no-img-element */}
          <img
            className="mb-4"
            srcSet="/images/lee_hite_welcome_sm.png 640w,/images/lee_hite_welcome.png 1080w"
            sizes="(max-width: 840px) 90vw,570px"
            src="/images/lee_hite_welcome.png"
            alt="A screenshot of Lee Hite’s chimes website, showing a heart-shaped photograph of two copper wind chime sets with the text “Say it with chimes” in the center, followed by the words “Welcome! Chime Design and Build By: Lee Hite” in large lettering."
          />
          <FigCaption>
            A screenshot of{" "}
            <Link href="https://leehite.org/Chimes.htm#39._Question:">
              Lee Hite’s beautiful website
            </Link>{" "}
            dedicated to the minutiae of designing and building wind chimes.
          </FigCaption>
        </Figure>
        <Paragraph>
          This website is a masterwork of documentation. It has information at
          every level of detail, from pre-built measurement tables to straight
          physics formulas. It has such a tremendous wealth of information that
          a community has formed around it, drawn by its author’s passion and
          commitment, providing even <em>more</em> information and tools.
        </Paragraph>
        <Paragraph>
          Lee’s website taught me several crucial things about wind chimes.
          First and foremost, it taught me the answer to Taylor’s question: The
          length and diameter do both matter, but not in the way I would have
          guessed!
        </Paragraph>
        <Paragraph>
          Increasing the length of the chime decreases the pitch. This makes
          intuitive sense to me, because the chime is essentially making sound
          by bending in half in alternating directions over and over again. This
          means that a longer chime will be producing a longer wavelength, which
          translates to a lower pitch. I was surprised to learn, however, that
          not only does increasing the diameter also effect the pitch, but it{" "}
          <em>also</em> lowers it. In retrospect, this also makes sense, I just
          didn’t have the correct mental model at the time. A larger diameter
          means more mass, more mass means more inertia, and more inertia means
          that the chime takes longer to slow down and speed up when it’s
          changing directions at the end of a bend. And more time to change
          directions means a longer wavelength and a lower pitch!
        </Paragraph>
        <Paragraph>
          So why were the lower-pitched chimes larger in diameter than the
          higher-pitched ones? It turns out, the answer is most likely that
          larger diameter (and thicker-walled) chimes just… sound better!
          Because they have more mass, they resonate for longer and have clearer
          harmonics. If you want to make a higher-pitched set of chimes, then
          you need less mass, as explained above, but apparently it’s otherwise
          almost always preferable to use wider diameter tubes.
        </Paragraph>
      </section>
      <section id="planning">
        <Paragraph>
          <LeadIn>Armed with this knowledge</LeadIn> — and a plethora more — it
          was time to start making some decisions. First, we needed to pick some
          pitches. To do this, I started out by playing some scales on a piano
          and had Taylor pick ones that she liked the sound of. We’d learned
          (through the website, of course!) that below C4, the chimes almost
          certainly wouldn’t have enough energy for a listener to hear the
          fundamental frequency, which results in the perception that the chime
          is producing a higher pitch. But we also wanted some low, resonant
          tones, so in the end we landed on a scale that started on G3 (only a
          bit below C4) and moved up the Mixolydian scale by thirds, forming a G
          dominant 9th chord. We spaced the notes out by thirds to try to avoid
          having pitches that were too close together, which might result in
          harmonics that clashed, creating a beating effect.
        </Paragraph>
        <Paragraph>
          Then it was time to a make a design and choose materials. We were a
          bit low on tools (though we ended up receiving a last minute toolkit
          loan from a friend who was excited about the project), and a lot of
          the designs seemed to be somewhat reliant on having a drill press at
          hand. So we tried to put together a design that would require, in
          essence, the least amount of precise drilling and fastening. We ended
          up choosing ¾ inch copper pipe for the chimes, tied to an 8-inch
          nickel-plated ring from and art supply store, with a slate coaster
          (for mass) fastened to a light wooden disk (also from the art supply
          store). Oh, and a copper-colored aluminum sheet for the wind sail!
        </Paragraph>
        <Paragraph>
          A member of the wonderful community that has formed around Lee’s
          website created{" "}
          <Link href="https://snyderfamily.com/chimecalcs/#">
            a simple calculator
          </Link>{" "}
          that we used for determining the length of each chime.
        </Paragraph>
        <Figure>
          <table className="mb-8 w-full">
            <thead>
              <tr className="border-b-2">
                <th className="text-left">Pitch</th>
                <th className="text-left">Frequency</th>
                <th className="text-left">Chime length</th>
                <th className="text-left">Hang point</th>
              </tr>
            </thead>
            <tbody>
              <tr className="border-b-2">
                <td className="py-2">G3</td>
                <td className="py-2">~192 Hz</td>
                <td className="py-2">27 ⅝”</td>
                <td className="py-2">6 3⁄16”</td>
              </tr>
              <tr className="border-b-2">
                <td className="py-2">B3</td>
                <td className="py-2">~242 Hz</td>
                <td className="py-2">24 ⅝”</td>
                <td className="py-2">5 ½”</td>
              </tr>
              <tr className="border-b-2">
                <td className="py-2">D4</td>
                <td className="py-2">~288 Hz</td>
                <td className="py-2">22 9⁄16”</td>
                <td className="py-2">5 1⁄16”</td>
              </tr>
              <tr className="border-b-2">
                <td className="py-2">F4</td>
                <td className="py-2">~343 Hz</td>
                <td className="py-2">20 11⁄16”</td>
                <td className="py-2">4 ⅝”</td>
              </tr>
              <tr className="border-b-2">
                <td className="py-2">A4</td>
                <td className="py-2">432 Hz</td>
                <td className="py-2">18 7⁄16”</td>
                <td className="py-2">4 ⅛”</td>
              </tr>
            </tbody>
          </table>
          <FigCaption>
            An astute reader might notice that these frequencies aren’t exactly
            what you’d expect for these pitches. Taylor was interested in
            experimenting with alternate tunings, so these are tuned to A4 at
            432 Hz, rather than A4 at 440 Hz, which is more standard for modern
            pop and folk music.
          </FigCaption>
        </Figure>
        <Paragraph>
          It even allowed us to enter the length that the pipe was sold in (in
          our case, 10 feet) so that we could see how many pipes to purchase and
          how much would be left over. This helped us decide on our pipe
          diameter — we specifically chose a ¾ inch pipe because it allowed
          these pitches to fit in a single 10-foot length!
        </Paragraph>
      </section>
      <section id="construction">
        <Paragraph>
          <LeadIn>At first,</LeadIn> construction was a breeze. Lee has created
          detailed videos walking through various parts of the construction
          process, and we set up a little factory line with myself, Taylor, and
          my wife as we cut pipes to length, filed away their sharp edges, and
          drilled holes to hang them on. In what felt like no time, we had our
          chimes, and were ready to hang them!
        </Paragraph>
        <Paragraph>
          In software engineering, there’s a rule called “the ninety-ninety
          rule”:
        </Paragraph>
        <BlockQuote>
          <Paragraph>
            The first 90 percent of the code accounts for the first 90 percent
            of the development time. The remaining 10 percent of the code
            accounts for the other 90 percent of the development time.
          </Paragraph>
          <Paragraph>— Tom Cargill, Bell Labs</Paragraph>
        </BlockQuote>
        <Paragraph>
          This is a clearly tongue-in-cheek “rule”, but the humor speaks to
          something real about building things. Humans are full to the brim with
          cognitive biases, and the way that we see the world tends to nudge us
          toward the assumption that if something <em>visually appears</em> to
          be close to completion, it probably is. In software, this might mean
          that when we’ve written 90% of the <em>code</em>, we perceive the
          problem as 90% solved. In chime-building, apparently, it can mean that
          when we have all the individual pieces of the chimes prepped and laid
          out, we perceive the chimes as 90% constructed. After all, all that
          was left was to tie some knots!
        </Paragraph>
        <Paragraph>
          In both scenarios, the “last 10%” is about putting the pieces
          together. This is so often where we find another 90% of work because
          this is where our assumptions are tested! When three engineers have
          been working on three separate modules, and it comes time to integrate
          them, that’s when we learn whether all three of those engineers have
          been working under the same set of assumptions. And when building wind
          chimes, and it’s time to fasten the pieces together, that’s when we
          learn what assumptions we’ve made about how these pieces will play
          with each other.
        </Paragraph>
      </section>
      <section id="assumptions">
        <Paragraph>
          <LeadIn>Sure enough,</LeadIn> I had made some assumptions, and not all
          of them were well-founded. Firstly, I had assumed that a simple
          overhand knot would be sufficient to fasten the chimes to the
          nickel-plated ring that we used as the support ring. Lee’s website
          mentioned rings as support systems only in passing, and had almost no
          detail about them, focusing instead on wooden support disks (which
          are, presumably, a better choice). We had gone with a ring because I
          was worried that — with our limited tools — we would struggle to drill
          perpendicular, evenly spaced holes in the support plate. I was now
          coming to regret this choice tremendously.
        </Paragraph>
        <Paragraph>
          My experience with knots is almost exclusively confined to rock
          climbing, where the rope materials, construction, and thickness are
          specifically engineered to hold knots when the rope is holding about
          two human beings worth of mass. I was woefully unprepared to attempt
          to transfer that knowledge to this new scenario, where a much thinner,
          simpler, and smoother cord was attempting to hold a knot with a heavy
          metal pipe attached to one end. The results were, if I’m being honest,
          devastating. No matter which knots I tied, the lightest jostling of
          the ring would send them all unraveling in moments. And more secure
          knots, like the figure eight, were very challenging to tie tightly at
          specific lengths in such a small cord.
        </Paragraph>
        <Paragraph>
          Finally, after hours of researching and trialing new knots, I landed
          on a double Davy knot. It held up very well under significant jostling
          and bouncing, and it was also relatively easy to tie very tightly
          against the ring itself, providing a decent amount of friction against
          sliding. This turned out to be very important, because I had made a
          second assumption: the knots would largely stay in place on the ring.
        </Paragraph>
        <Paragraph>
          This second assumption proved even more dangerous than the first.
          While enough wind would eventually jostle the chimes enough to loosen
          the knots I had tried before the double Davy, they tended to encounter
          a similarly catastrophic failure far before that happened. After only
          the slightest adjustment in the ring’s balance, which would happen
          essentially every time a moderate gust blew, all the knots would
          rapidly slide around the ring to a single point. This would sometimes
          be enough acceleration to then trigger the knots to come undone, but
          even when it didn’t, the result was a completely useless set of
          chimes.
        </Paragraph>
        <Paragraph>
          I was so surprised by this initially because I had failed to imagine
          the chimes set as a dynamic system. A slight tilt in the support ring
          led to one or two knots sliding a few millimeters — on its own, this
          was no problem. But that slide redistributed the weight on the ring,
          causing it to tilt further in the same direction, which led to more
          sliding, then more tilting, until the ring was essentially vertical,
          and the knots were falling at speed toward the bottom point.
        </Paragraph>
        <Paragraph>
          As mentioned, the first step toward resolving this issue was to find
          knots that provided enough friction to stay in place during a slight
          tilt. That was accomplished with the double Davy knot. The second step
          was to limit the amount that the ring could tilt, so that we could
          avoid the cascade. Eventually, after quite a bit more trial and error,
          this was accomplished by attaching the support hook (the single point
          that the entire set would hang from) to the ring with five more cords,
          their knots interspersed within the knots holding the chimes.
        </Paragraph>
      </section>
      <section id="conclusion">
        <Paragraph>
          <LeadIn>Then, suddenly,</LeadIn> the chimes worked. I held up the set,
          gave the wind sail a nudge, and then… music.
        </Paragraph>
        <Figure>
          {/* eslint-disable-next-line @next/next/no-img-element */}
          <img
            className="mb-4"
            src="/images/chimes_complete.png"
            alt="A photograph of the chimes, hanging on Taylor’s porch. There are five chimes of varying lengths, hung from a metal hoop. In the center of the chimes is a wooden striker. In the background, there’s a wooded area, with a hammock hung between two trees."
          />
          <FigCaption>
            The completed chimes, hanging on Taylor’s front porch.
          </FigCaption>
        </Figure>
      </section>
    </article>
  );
}

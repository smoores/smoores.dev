import { BlockQuote } from "@/components/BlockQuote";
import { DateLine } from "@/components/DateLine";
import { H3 } from "@/components/H3";
import { Link } from "@/components/Link";
import { Paragraph } from "@/components/Paragraph";
import { UnderlinedHeading } from "@/components/UnderlinedHeading";
import { Metadata } from "@/metadata";

export const metadata: Metadata = {
  slug: "back_to_basics",
  date: "Feb. 28, 2023",
  title: "Back to Basics",
  description:
    "The Oak Team, the engineering team responsible for building and maintaining The New York Times' collaborative rich text editor, just had the most engaging, fulfilling sprint that I've experienced since I joined the team in 2019. We collaborated, supported each other, and learned new things. And we didn't push a single commit. Instead, every engineer on the team spent two weeks learning about how the core technologies we use every day really work, so that we could engage with them more deeply, and hopefully solve some long-standing problems we face while developing Oak!",
};

export function BackToBasics() {
  return (
    <article>
      <UnderlinedHeading>{metadata.title}</UnderlinedHeading>
      <DateLine date={metadata.date} />
      <section id="intro">
        <Paragraph>
          The Oak Team, the engineering team responsible for building and
          maintaining The New York Times’ collaborative rich text editor, just
          had the most engaging, fulfilling sprint that I’ve experienced since I
          joined the team in 2019. We collaborated, supported each other, and
          learned new things. And we didn’t push a single commit. Instead, every
          engineer on the team spent two weeks learning about how the core
          technologies we use every day really work, so that we could engage
          with them more deeply, and hopefully solve some long-standing problems
          we face while developing Oak!
        </Paragraph>
        <Paragraph>
          What we sketched out became{" "}
          <Link
            href="https://nytimes.github.io/oak-byo-react-prosemirror-redux"
            rel="noopener noreferrer"
          >
            “Build Your Own”
          </Link>
          . “Build Your Own” is a five-part syllabus that breaks down React,
          ProseMirror, and Redux, and walks through how to build them back up
          from scratch. Let’s talk about what it is and why we need it!
        </Paragraph>
      </section>
      <section id="context">
        <H3>What Oak is made of</H3>
        <Paragraph>
          Before we talk about the learning sprint itself, some context. For
          quite a while now,{" "}
          <Link
            href="https://open.nytimes.com/react-relay-and-graphql-under-the-hood-of-the-times-website-redesign-22fb62ea9764"
            rel="noopener noreferrer"
          >
            the primary UI rendering library at The Times has been React
          </Link>
          . Since Oak is a{" "}
          <Link
            href="https://en.wikipedia.org/wiki/WYSIWYG"
            rel="noopener noreferrer"
          >
            WYSIWYG text editor
          </Link>
          , it shares React component code with our reader-facing website at{" "}
          <Link href="https://www.nytimes.com" rel="noopener noreferrer">
            nytimes.com
          </Link>
          . That way, when changes are made to how articles look on our website,
          our newsroom can also see those changes while the articles are being
          written!{" "}
          <Link
            href="https://open.nytimes.com/building-a-text-editor-for-a-digital-first-newsroom-f1cb8367fc21"
            rel="noopener noreferrer"
          >
            Oak also relies on ProseMirror
          </Link>
          , a library for building rich text editors. These two technologies,
          React and ProseMirror, introduce an interesting tension: they both
          want to have complete ownership over the DOM they’re responsible for,
          and in many places they’re responsible for the same DOM! Mediating
          this tension is one of the Oak team’s primary technical challenges,
          especially when working on features that involve decorating the
          editor, like spellcheck.
        </Paragraph>
      </section>
      <section id="a-step-back">
        <H3>Taking a step back</H3>
        <Paragraph>
          Thinking about challenges like this is one of my favorite aspects of
          my job as a tech lead. As I thought about this tension in particular,
          I began to see what made it murkier than many of our team’s other
          technical challenges: we don’t usually expect this knowledge from
          engineers that build applications. Most of the time, it’s possible to
          treat libraries like ProseMirror and React as abstractions. It’s
          almost always enough to just understand how to use our tools, and we
          leave understanding how they’re built—and why—to the domain experts.
          But the fraught relationship between React and ProseMirror within the
          Oak codebase has given rise to frustrating and resilient bugs over the
          5 years that we’ve been building it. Simply knowing how to use our
          tools was not enough.
        </Paragraph>
        <Paragraph>
          This is how we ended up developing our learning sprint. To deviate
          from this standard mode of operation, our team had to think carefully
          about how to ensure that everyone was prepared to take on this work.
          We had to build a solid, shared foundation so that we weren’t leaving
          anyone behind, and so that we could make the most of our collaboration
          as a team. This, I realized, was the real challenge: increasing our
          base level understanding of how React, ProseMirror, and Redux are
          actually implemented, so that every engineer on the team can engage
          thoughtfully in solving the problems that arise when these libraries
          are in conflict.
        </Paragraph>
      </section>
      <section id="build-your-own">
        <H3>Build Your Own</H3>
        <Paragraph>
          What we sketched out became{" "}
          <Link
            href="https://nytimes.github.io/oak-byo-react-prosemirror-redux"
            rel="noopener noreferrer"
          >
            “Build Your Own”
          </Link>
          . “Build Your Own” is a five-part syllabus that breaks down React,
          ProseMirror, and Redux, and walks through how to build them back up
          from scratch. It’s based on (and includes) the absolutely wonderful{" "}
          <Link
            href="https://pomb.us/build-your-own-react/"
            rel="noopener noreferrer"
          >
            “Build your own React” tutorial from Rodrigo Pombo
          </Link>
          . Inspired by that tutorial, we wrote similar walkthroughs for
          building Redux and the ProseMirror EditorView component as well.
          Finally, to ensure that everyone felt comfortable with the terminology
          and fundamentals of using the library, we built quick refresher
          courses on the basics of React and ProseMirror.
        </Paragraph>
        <Paragraph>
          Since the primary goal of this project was to ensure that everyone had
          the same foundational knowledge of these three technologies before
          embarking on projects to improve how we use them, it was crucial that
          the actual learning process was flexible enough to accommodate the
          different learning styles across our team. We made space for learning
          through dialogue, via the synchronous Slack Huddle, through reading,
          via the tutorials, and through experimentation, and via the Glitch
          projects we created for each course. We also incorporated feedback as
          we went, updating the courses and our processes to ensure they were as
          useful as possible.
        </Paragraph>
        <Paragraph>
          We went through this learning sprint as a team in May of 2022. We’ve
          since used our shared knowledge to reimagine the way we integrate
          React, ProseMirror, and Redux in the Oak collaborative text editor.
          Every engineer on the team has been able to contribute to the project,
          including the two engineers who joined the team since we first wrote
          the course. Together we wrote forty-five tickets, and less than two
          months after the first one was opened, the refactor was live in
          production. We were able to build a new integration layer between
          React and ProseMirror that utilizes a deep understanding of
          ProseMirror NodeViews and React render phases, layout effects,
          contexts, and portals to create a clean, bug-free system that will
          hopefully serve us for the rest of Oak’s life. Keep an eye out,
          because we’re hoping to open source and release this new ProseMirror
          React integration library some time in 2023!
        </Paragraph>
        <Paragraph>
          Overall, the learning sprint was a blast, and, I think, a fairly
          resounding success. The change was noticeable and instant: our team
          was much more able to engage with challenging conversations about each
          of these technologies. And folks had a great time, too!
        </Paragraph>
        <Paragraph>
          Some testimonials from engineers on the Oak team about our learning
          sprint:
        </Paragraph>
        <BlockQuote>
          I loved that our Oak learning sprint accommodated the many learning
          and working styles on our team, with options to work asynchronously or
          together in a Slack hHuddle and flexibility for every engineer’s
          personal pace through the tutorials. The work we do at the Times is
          never-ending, which just made it that much more meaningful that as a
          team, we were willing to take a week off from our normal sprint work
          to make sure we all understand what is going on behind the scenes in
          the tools we use every day. I know that when we start to tackle our H2
          work, the learning sprint will continue to help us.
        </BlockQuote>
        <Paragraph>- Nozlee Samadzadeh</Paragraph>
        <BlockQuote>
          The Prosemirror-React learning week was extremely valuable to me as a
          mid-level engineer in that it forced me to examine the building blocks
          of the two main libraries we use to build Oak. Doing so allowed me to
          make sense of the patterns we use in our code, and the vocabulary to
          debug more effectively. Re-assembling Prosemirror-view and React
          demystified their mechanics and made me feel more empowered in
          leveraging their features.
        </BlockQuote>
        <Paragraph>- Anna Bialas</Paragraph>
      </section>
    </article>
  );
}

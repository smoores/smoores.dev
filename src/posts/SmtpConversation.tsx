import { DateLine } from "@/components/DateLine";
import { H2 } from "@/components/H2";
import { H3 } from "@/components/H3";
import { InlineCode } from "@/components/InlineCode";
import { Link } from "@/components/Link";
import { Paragraph } from "@/components/Paragraph";
import { UnderlinedHeading } from "@/components/UnderlinedHeading";
import { Metadata } from "@/metadata";
import { Code } from "bright";

export const metadata: Metadata = {
  slug: "smtp_conversation",
  date: "May 9, 2020",
  title: "SMTP: A Conversation",
  description:
    "One crucial piece of network administration has always evaded me. Past the routers, VPNs, websites and Docker images, always just out reach, sits the big one: email. I have never been able to wrap my head around even the simplest pieces of email networking. Somehow, this fundamental part of the modern internet has always just been a little bit too complex, just a little bit too finicky.",
};

export function SmtpConversation() {
  return (
    <article>
      <UnderlinedHeading>{metadata.title}</UnderlinedHeading>
      <DateLine date={metadata.date} />
      <section id="intro">
        <Paragraph>
          Over the past five years or so, I’ve slowly been building up my
          homelab. What was once just a pihole and a Plex server has evolved
          into a fairly substantial network of over 40 different services and
          websites, all run out of a few servers on my home network. It’s been
          one of the best learning opportunities I’ve ever had, covering Docker,
          CSP, reverse proxies, DNS, and too much more to list here. Overall, I
          would highly recommend it.
        </Paragraph>
        <Paragraph>
          One crucial piece of network administration has always evaded me,
          though. Past the routers, VPNs, websites and Docker images, always
          just out reach, sits the big one: <strong>email</strong>. I have never
          been able to wrap my head around even the simplest pieces of email
          networking. Somehow, this fundamental part of the modern internet has
          always just been a <em>little</em>
          bit too complex, just a <em>little</em> bit too finicky.
        </Paragraph>
      </section>
      <section>
        <H3 id="enough-is-enough">Enough Is Enough</H3>
        <Paragraph>
          This morning when I sat down at my computer, for reasons I can’t fully
          explain, I decided that I’d had it. Today was the day. I was going to
          get my email properly set up. It was getting ridiculous; I’m a web
          developer! I even built a simple SMTP server in college for an
          assignment. I understand the basics, why couldn’t I put them into
          practice?
        </Paragraph>
        <Paragraph>
          It’s at this point where it might make sense to step back and explain
          the specific problem I was trying to solve. I have no interest in
          hosting my own email server (well, not <em>no</em> interest, but I’m
          not quite there yet), but I do have a{" "}
          <Link href="#nextcloud">Nextcloud</Link> instance. Nextcloud is a self
          hosted Google Drive alternative, and I have it running in a{" "}
          <Link href="#docker">Docker</Link> container on my{" "}
          <Link href="#unraid">Unraid</Link> server. Like any good cloud file
          storage service, Nextcloud lets you share your files with other users
          on your instance, or send out share links by email. All you need to do
          is configure your Nextcloud instance to point to an{" "}
          <Link href="#smtp">SMTP</Link> server, like the one you use for your
          normal hosted email, and you’re all set.
        </Paragraph>
        <Paragraph>
          This is, unfortunately for me, where it gets tricky. I use{" "}
          <Link rel="noopener noreferrer" href="https://protonmail.com/">
            ProtonMail
          </Link>{" "}
          as my primary email provider. ProtonMail provides an end-to-end
          encrypted email service, which means that your emails are always
          encrypted on your end before they’re even sent to ProtonMail’s
          servers. They don’t actually expose their SMTP service to their
          customers directly, because then dumb “clients” like Nextcloud, that
          don’t know anything about encrypted email, would just send them emails
          in plaintext! To get around this problem, ProtonMail provides a piece
          of software called ProtonMail Bridge. It runs on your computer and
          exposes a local SMTP server that receives emails from your mail client
          (like the Apple Mail app or Mozilla Thunderbird, or in my case,
          Nextcloud) and encrypts them before sending them to ProtonMail’s
          servers. Until recently, ProtonMail Bridge was in beta for Linux, but
          it was finally released (in a <em>much</em> more polished state) to
          all of their customers about a month ago.
        </Paragraph>
      </section>
      <section>
        <H3 id="and-so-it-begins">And So It Begins</H3>
        <Paragraph>
          Great, I thought to myself, as the sunrise peeked in through the
          blinds. All I need to do is get the Bridge set up on my Unraid server
          and point Nextcloud at it and I’ll be ready to roll! Well, I can’t set
          it up <em>on</em> my Unraid server, or at least not directly. Unraid
          is a fork of <Link href="#slackware">Slackware</Link>, and the OS runs
          entirely in memory, so if I want to install Linux software that
          persists between reboots I need to use a virtual machine. Not a
          problem, I thought, feeling the type of confidence you only feel about
          15 minutes into a new project. I already set up a Debian VM to run{" "}
          <Link href="#youtube-dl">youtube-dl</Link>, I’ll just use that.
        </Paragraph>
        <Paragraph>
          I logged in to my VM, downloaded the package installer, and with a{" "}
          <InlineCode>sudo apt install ./protonmail-bridge.deb</InlineCode>, I
          was off to the races. Sort of. As I mentioned, for a while ProtonMail
          Bridge for Linux was in a beta program, which I participated in, so at
          this point I was fairly well practiced in getting it set up correctly
          on a headless server. This is something it wasn’t quited designed for
          initially, though the latest release does seem to have some better
          support for it. So I also installed <Link href="#pass">GNU Pass</Link>
          , because the Bridge needs a keychain to store some tokens in, and I
          generated a new <Link href="#gpg">GPG key</Link> to initialize the{" "}
          <InlineCode>protonmail-bridge</InlineCode> folder in{" "}
          <InlineCode>pass</InlineCode>. As I said, this isn’t the first time
          I’ve done this, so it only took 4 or 5 tries this time and I managed
          to get it working <em>just</em> before the headache set in.
        </Paragraph>
        <Paragraph>
          So far, you might think, things seem to be going smoothly. Far too
          smoothly, in fact, to justify the word count of this post. Where’s the
          drama, the intrigue? Well, my sadistic friend, this is where it
          begins. ProtonMail Bridge, probably for security reasons, binds to the
          host <InlineCode>127.0.0.1</InlineCode> when it starts. It’s actually
          hard-coded to do so, it can’t be configured to find to anything else,
          which means that it can’t be reached from over the network, only from
          other services running on the same computer. This wouldn’t be an issue
          (remember, technically Bridge and Nextcloud are running on the same
          computer), except that Nextcloud is running in a Docker container and
          Bridge is running in a VM, so they <em>think</em> that they’re on
          different machines, which is unfortunately all that matters.
        </Paragraph>
      </section>
      <section>
        <H3 id="uphill-from-here">Uphill From Here</H3>
        <Paragraph>
          This is about the moment that my palms start sweating, as I realize
          that I might need to take off work for the next week to pore over the
          docs for <Link href="#sendmail">sendmail</Link> and learn the entirety
          of email administration just to blindly forward some mail to
          ProtonMail. But I manage to keep it together long enough to stumble
          upon{" "}
          <Link
            rel="noopener noreferrer"
            href="http://emailrelay.sourceforge.net/"
          >
            E-mailRelay
          </Link>
          , an email proxy server. This is exactly what I need! It has great
          documentation and looks like it’s kept fairly up-to-date. I
          frantically downloaded and installed it, feeling very, very greatful
          for the author of this extremely well documented default config file
          as I flipped some switches and turned on the service with
          <InlineCode>sudo service emailrelay start</InlineCode>. After some
          brief difficulties getting TLS working between Nextcloud and
          E-mailRelay (followed by turning it off, it’s staying on my LAN
          anyway!), I was ready to test it out. Heart racing, I clicked the
          “Send Test” button in my Nextcloud admin panel and I see the
          successful logs in my terminal and...
        </Paragraph>
        <Code
          theme="dracula"
          lang="js"
        >{`emailrelay: info: smtp connection from 192.168.1.73:35962
emailrelay: info: smtp connection closed: smtp protocol done: 192.168.1.73:35962
emailrelay: info: smtp connection to 127.0.0.1:1025
emailrelay: warning: client protocol: unexpected response [Was expecting MAIL arg syntax of FROM:<address>]
emailrelay: info: failing file: "emailrelay.13631.1589033674.3.envelope.busy" -> "emailrelay.13631.1589033674.3.env
emailrelay: error: forwarding: smtp error: unexpected response: Was expecting MAIL arg syntax of FROM:<address>`}</Code>
        <Paragraph>
          Wait a minute. What’s going on at the end there? That’s an error
          message. And it looks like an error with the syntax that E-mailRelay
          is using to talk to ProtonMail Bridge? That seems like a Big Deal™,
          like if nginx was messing up HTTP. I needed to dig deeper and try to
          understand what was happening, so I installed{" "}
          <Link rel="noopener noreferrer" href="https://www.tcpdump.org/">
            tcpdump
          </Link>{" "}
          to inspect the communications between the two services. Since the
          communication was taking place within the VM, I told tcpdump to watch
          the “loopback” interface and log any packets as ASCII:
        </Paragraph>
        <Code theme="dracula" lang="js">{`$ sudo tcpdump -i lo -A
Bridge > 220 127.0.0.1 ESMTP Service Ready
EmailRelay > EHLO debian.r710
Bridge > 250-Hello debian.r710
Bridge > 250 AUTH PLAIN LOGIN
EmailRelay > AUTH PLAIN [auth token redacted]
Bridge > 235 Authentication succeeded
EmailRelay > MAIL FROM:<email@example.com> BODY=7BIT AUTH=<email@example.com>
Bridge > 501 Was expecting MAIL arg syntax of FROM:<address>`}</Code>
        <Paragraph>
          The great thing about SMTP (Simple Mail Transfer Protocol) is that
          it’s a plaintext protocol that reads like conversation. It’s clear
          here that for some reason Bridge doesn’t consider the MAIL syntax to
          be correct. My first question is, of course, is it correct? Time to
          look through some <Link href="#rfc">RFCs</Link>.
        </Paragraph>
        <Paragraph>
          <Link
            rel="noopener noreferrer"
            href="https://tools.ietf.org/html/rfc5321"
          >
            RFC 5321
          </Link>
          lays out the specification for the Simple Mail Transfer Protocol, and{" "}
          <Link
            rel="noopener noreferrer"
            href="https://tools.ietf.org/html/rfc5321#section-3.3"
          >
            section 3.3
          </Link>{" "}
          describes the syntax for the MAIL command.
        </Paragraph>
        <Code
          theme="dracula"
          lang="js"
        >{`MAIL FROM:<reverse-path> [SP <mail-parameters> ] <CRLF>`}</Code>
        <Paragraph>
          Good news, I guess; the command looks fine! The MAIL command can
          specify AUTH and BODY parameters after the FROM, so it seems like the
          issue is on the Bridge side, not E-mailRelay. Time to dig deeper!
        </Paragraph>
        <Paragraph>
          Luckily, ProtonMail Bridge is{" "}
          <Link
            rel="noopener noreferrer"
            href="https://github.com/ProtonMail/proton-bridge"
          >
            open source
          </Link>
          , along with the rest of ProtonMail’s client software. Unluckily it’s
          written in Go, which is not a language I’m very familiar with. It’s at
          this point where I started making some mistakes. I saw that ProtonMail
          Bridge depends on{" "}
          <Link
            rel="noopener noreferrer"
            href="https://github.com/emersion/go-smtp"
          >
            go-smtp
          </Link>
          , an alternative to net/smtp that provides a server implementation and
          is actively maintained, so I pulled down the code and started reading.
          And sure enough, there was my error message!
        </Paragraph>
        <Code
          lang="cpp"
          theme="dracula"
        >{`if len(arg) < 6 || strings.ToUpper(arg[0:5]) != "FROM:"
{
  c.WriteResponse(501, EnhancedCode{5, 5, 2}, "Was expecting MAIL arg syntax of FROM:<address>")
  return
}

fromArgs := strings.Split(strings.Trim(arg[5:], " "), " ")

if c.server.Strict
{
  if !strings.HasPrefix(fromArgs[0], "<") || !strings.HasSuffix(fromArgs[0], ">")
  {
    c.WriteResponse(501, EnhancedCode{5, 5, 2}, "Was expecting MAIL arg syntax of FROM:<address>")
    return
  }
}

from := fromArgs[0]

if from == ''
{
  c.WriteResponse(501, EnhancedCode{5, 5, 2}, "Was expecting MAIL arg syntax of FROM:<address>")
  return
}`}</Code>
        <Paragraph>
          In my excitement, I slightly misread this second condition. I skimmed
          right over the part where <InlineCode>arg</InlineCode> is split by
          spaces, so when I saw
          <InlineCode>
            !strings.<em>HasSuffix</em>(fromArgs[0], “&gt;”)
          </InlineCode>{" "}
          I thought I’d found it. Clearly, this library wasn’t expecting any of
          the optional parameters after the FROM arg; it was expecting the last
          character in the command to be a <InlineCode>&gt;</InlineCode>. I
          didn’t own this library, or feel very confident about my ability to
          muddle with the ProtonMail Bridge source code without breaking
          anything, but I <em>did</em>
          have the E-mailRelay source code, which seemed a lot safer to mess
          around with.
        </Paragraph>
        <Paragraph>
          E-mailRelay is written in C++, a language I’m only just slightly more
          familiar than Go, but luckily SMTP clients are pretty straightforward.
          It didn’t take long to find where the MAIL command was being
          constructed.
        </Paragraph>
        <Code
          theme="dracula"
          lang="cpp"
        >{`void GSmtp::ClientProtocol::sendMailCore()
{
  std::string mail_from_tail = message()->from() ;
  mail_from_tail.append( 1U , '>' ) ;
  if( m_server_has_8bitmime && message()->eightBit() != -1 )
  {
    mail_from_tail.append( message()->eightBit() ? " BODY=8BITMIME" : " BODY=7BIT" ) ; }
  if( m_authenticated_with_server && message()->fromAuthOut().empty() && !m_sasl->id().empty() )
  {
    &sol;&sol; default policy is to use the session authentication id, although
    &sol;&sol; this is not strictly conforming with RFC-2554
    mail_from_tail.append( " AUTH=" ) ;
    mail_from_tail.append( G::Xtext::encode(m_sasl->id()) ) ;
  }
  else if( m_authenticated_with_server && G::Xtext::valid(message()->fromAuthOut()) )
  {
    mail_from_tail.append( " AUTH=" ) ;
    mail_from_tail.append( message()->fromAuthOut() ) ;
  }
  else if( m_authenticated_with_server )
  {
    mail_from_tail.append( " AUTH=<>" ) ;
  }
  send( "MAIL FROM:<" , mail_from_tail ) ;
}`}</Code>
        <Paragraph>
          I had a hunch that it would be fine to just drop the optional
          parameters entirely, so that was the first step. I cut out all of the
          BODY and AUTH logic from the <InlineCode>sendMailCore</InlineCode>{" "}
          function until all that remained was this:
        </Paragraph>
        <Code
          theme="dracula"
          lang="cpp"
        >{`void GSmtp::ClientProtocol::sendMailCore()
{
  std::string mail_from_tail = message()->from() ;
  mail_from_tail.append( 1U , '>' ) ;
  send( "MAIL FROM:<" , mail_from_tail ) ;
}`}</Code>
        <Paragraph>
          The E-mailRelay project, which I’m becoming more impressed with by the
          minute at this point, has support for installing locally as a{" "}
          <InlineCode>.deb</InlineCode> package, so testing this out was as
          simple as{" "}
          <InlineCode>
            ./configure && make deb && sudo apt install ./emailrelay_2.1.deb
          </InlineCode>
          . And lo and behold, it worked! Simply dropping those parameters from
          the MAIL command allowed the Bridge to parse the command correctly,
          and didn’t seem to cause any other issues with mail delivery.
        </Paragraph>
      </section>
      <section>
        <H3 id="denoument">Denoument</H3>
        <Paragraph>
          At this point my system is working, though it doesn’t feel great to be
          relying on a fork of E-mailRelay, especially since my tweak shouldn’t
          have been necessary in the first place! Ideally, ProtonMail Bridge
          could be updated to properly handle these parameters. And I wasn’t
          alone here; though I might be the only person with this specific set
          of requirements, during my saga I stumbled upon a{" "}
          <Link
            rel="noopener noreferrer"
            href="https://github.com/ProtonMail/proton-bridge/issues/46"
          >
            reddit post from three months prior
          </Link>{" "}
          where someone was experiencing the exact same issue with KMail, KDE’s
          email client.
        </Paragraph>
        <Paragraph>
          My first stop was go-smtp, because I was still under the (slightly
          misguided) impression that it was being used as the SMTP server
          interface in ProtonMail Bridge. I{" "}
          <Link
            rel="noopener noreferrer"
            href="https://github.com/emersion/go-smtp/issues/95"
          >
            opened an issue
          </Link>{" "}
          explaining what I was experiencing. Within minutes, a contributor
          responded and started digging. It turns out that I hadn’t quite gotten
          it right; adding an AUTH parameter <em>did</em> cause the server to
          incorrectly throw an error, but it was a different error than the one
          I was seeing! And while ProtonMail Bridge was using go-smtp, it turns
          out it was using a <em>very</em> stale fork from 2018.
        </Paragraph>
        <Paragraph>
          The contributor had a fix for the current issue ready and merged
          within an hour, which was outstanding. Unfortunately, this fix would
          never make it to ProtonMail Bridge if they didn’t at least update
          their fork!
          <Link
            rel="noopener noreferrer"
            href="https://github.com/ProtonMail/proton-bridge/issues/46"
          >
            Time for another
          </Link>{" "}
          issue, this one in the Bridge repo, and armed with a lot more
          information. With any luck, fixing this will be as simple as pulling
          in the latest go-smtp!
        </Paragraph>
        <Paragraph>
          This rabbit hole consumed just about my entire day, but on the whole
          it actually
          <em>energized</em> me. It feels good to be able to contribute to these
          projects like ProtonMail Bridge and go-smtp, even if it’s just by
          identifying and flagging bugs in a useful way. And it feels good to
          have enough working knowledge to patch an issue locally so that it
          doesn’t break my own system! Everything about web development’s
          foundations, from open protocols like SMTP to the RFC process supports
          this kind of contribution, and I’m finally starting to be able to take
          part.
        </Paragraph>
      </section>
      <section>
        <H3 id="epilogue">Epilogue</H3>
        <DateLine date="May 11, 2020" />
        <Paragraph>
          With some fresh eyes, I took another look at{" "}
          <Link
            rel="noopener noreferrer"
            href="https://github.com/ProtonMail/proton-bridge/blob/8288a39ff427dccb3db66048240393084db2b23b/go.mod#L76"
          >
            ProtonMail Bridge’s go.mod
          </Link>{" "}
          file this morning. I noticed that they were specifying a commit to
          depend on, and after some digging learned that that commit was
          actually much older than I thought it was! It wasn’t using the code I
          shared above at all, instead it was using a regex to validate the MAIL
          command:
        </Paragraph>
        <Code
          theme="dracula"
          lang="go"
        >{`re := regexp.MustCompile("(?i)^FROM:\\s*<((?:\\\\>|[^>])+|\"[^\"]+\"@[^>]+)>( [\\w= ]+)?$")
m := re.FindStringSubmatch(arg)`}</Code>
        <Paragraph>
          This regex is <em>so</em> close to working, but it exclusively expects
          word characters in the optional parameters (because of the{" "}
          <InlineCode>\w</InlineCode>), which means that it breaks when the
          client sends an AUTH parameter like{" "}
          <InlineCode>AUTH=&lt;email@example.com&gt;</InlineCode>.
        </Paragraph>
        <Paragraph>
          The very, very good news is that ProtonMail responded to that issue,
          and{" "}
          <Link
            rel="noopener noreferrer"
            href="https://github.com/ProtonMail/proton-bridge/issues/46#issuecomment-626663151"
          >
            they’re planning on updating to the latest go-smtp
          </Link>
          !
        </Paragraph>
      </section>
      <aside>
        <H2>Glossary</H2>
        <H3 id="nextcloud">
          <Link rel="noopener noreferrer" href="https://nextcloud.com">
            Nextcloud
          </Link>
        </H3>
        <Paragraph>
          Nextcloud is a free and open source suite of software for file
          hosting. It’s a self-hostable alternative to Google Drive, and
          includes an extension ecosystem that supports everything from calendar
          to video chat.
        </Paragraph>
        <H3 id="docker">
          <Link rel="noopener noreferrer" href="https://www.docker.com/">
            Docker
          </Link>
        </H3>
        <Paragraph>
          Docker refers to a containerization technology developed for Linux
          (that can run on other operating systems through virtual machine
          software) that allows you to run software in “containers” or “jails”.
          Each container believes it has access to an entire machine ( similar
          to a virtual machine, but with much less overhead), and this system
          allows you to easily run multiple programs that would otherwise have
          conflicting dependencies or require complex permissions management of
          shared data.
        </Paragraph>
        <H3 id="unraid">
          <Link rel="noopener noreferrer" href="https://www.unraid.net/">
            Unraid
          </Link>
        </H3>
        <Paragraph>
          Unraid is an operating system designed to run on a Network Attached
          Storage (NAS) server. It has support for data redundancy via one or
          two “parity” disks, and has a web interface that allows you to easily
          manage Docker containers and virtual machines.
        </Paragraph>
        <H3 id="smtp">
          <Link
            rel="noopener noreferrer"
            href="https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol"
          >
            SMTP
          </Link>
        </H3>
        <Paragraph>
          SMTP stands for Simple Mail Transfer Protocol. It’s an internet
          protocol for email, and is the modern standard protocol used for all
          email communications (analogous to HTTP for website communication).
          It’s a text-based protocol and designed to be easily read by humans.
        </Paragraph>
        <H3 id="slackware">
          <Link rel="noopener noreferrer" href="http://www.slackware.com/">
            Slackware
          </Link>
        </H3>
        <Paragraph>
          Slackware is a Linux distrobution. In fact, it’s the oldest Linux
          distrobution that’s still maintained. It aims to be the most
          “Unix-like” Linux distrobution. It doesn’t provide any dependency
          resolution for software packages, which means installing software can
          be an intensely manual process.
        </Paragraph>
        <H3>
          <Link rel="noopener noreferrer" href="https://youtube-dl.org">
            youtube-dl
          </Link>
        </H3>
        <Paragraph>
          <InlineCode>youtube-dl</InlineCode> is a command-line program for
          downloading videos from YouTube.com.
        </Paragraph>
        <H3 id="pass">
          <Link rel="noopener noreferrer" href="https://www.passwordstore.org/">
            pass
          </Link>
        </H3>
        <Paragraph>
          Pass is a Unix password manager. Each password lives inside a GPG
          encrypted file, and the store itself is just a directory, making
          organizing and maintaining your passwords and secrets as easy as
          moving some files around.
        </Paragraph>
        <H3 id="gpg">
          <Link rel="noopener noreferrer" href="https://gnupg.org/">
            GPG
          </Link>
        </H3>
        <Paragraph>
          GPG, or GnuPG, stands for GNU Privacy Guard (GNU itself stands for
          GNU’s Not Linux). It’s a free and open source cryptographic encryption
          suite that implements OpenPGP, the Internet Standards specficiation
          for “Pretty Good Privacy”.
        </Paragraph>
        <H3 id="sendmail">
          <Link
            rel="noopener noreferrer"
            href="https://linux.die.net/man/8/sendmail.sendmail"
          >
            Sendmail
          </Link>
        </H3>
        <Paragraph>
          Sendmail is an outstandingly well-named software package for sending
          email. It implements SMTP, along with other mail transfer protocols.
          It’s also known for it’s flexible, expressive, and, some might say,
          headache-inducing configuration spec.
        </Paragraph>
        <H3 id="rfc">
          <Link
            rel="noopener noreferrer"
            href="https://en.wikipedia.org/wiki/Request_for_Comments"
          >
            RFC
          </Link>
        </H3>
        <Paragraph>
          RFC stands for Request For Comments. It’s the primary format used for
          communicating standards across various Internet Standards committees
          and groups.
        </Paragraph>
      </aside>
    </article>
  );
}

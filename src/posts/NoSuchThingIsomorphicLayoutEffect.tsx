import { focus } from "@/brightExtensions/focus";
import { BlockQuote } from "@/components/BlockQuote";
import { DateLine } from "@/components/DateLine";
import { InlineCode } from "@/components/InlineCode";
import { LeadIn } from "@/components/LeadIn";
import { Link } from "@/components/Link";
import { Paragraph } from "@/components/Paragraph";
import { UnderlinedHeading } from "@/components/UnderlinedHeading";
import { Metadata } from "@/metadata";
import { Code } from "bright";

export const metadata: Metadata = {
  title: "There’s no such thing as an isomorphic layout effect",
  slug: "no_such_thing_isomorphic_layout_effect",
  date: "Feb. 22, 2025",
  description:
    "There’s something off about the most popular approach to quieting React’s warnings about using layout effects during server-side rendering. Let’s break down why the warning exists, and when it really makes sense to disable it without actually addressing it.",
};

export function NoSuchThingIsomorphicLayoutEffect() {
  Code.extensions = [focus];

  return (
    <article>
      <UnderlinedHeading>{metadata.title}</UnderlinedHeading>
      <DateLine date={metadata.date} />
      <section id="intro">
        <Paragraph>
          So, recently,{" "}
          <Link href="https://github.com/handlewithcarecollective/react-prosemirror">
            React ProseMirror
          </Link>{" "}
          added support for server-side rendering. If you read{" "}
          <Link href="/post/why_i_rebuilt_prosemirror_view">
            my post about how React ProseMirror works
          </Link>
          , you may already know that React ProseMirror relies fairly heavily on
          React’s <InlineCode>useLayoutEffect</InlineCode> hook for reading data
          from the DOM after render. And if you’re familiar with server-side
          rendering, you may be familiar with what happens when you render a
          component that uses <InlineCode>useLayoutEffect</InlineCode> on the
          server:
        </Paragraph>
        <Code>{`Warning: useLayoutEffect does nothing on the server, because its effect
cannot be encoded into the server renderer's output format. This will
lead to a mismatch between the initial, non-hydrated UI and the intended
UI. To avoid this, useLayoutEffect should only be used in components
that render exclusively on the client. See
https://reactjs.org/link/uselayouteffect-ssr for common fixes.`}</Code>
      </section>
      <section id="breakdown">
        <Paragraph>
          <LeadIn>It’s worth</LeadIn> breaking down what this warning is
          actually trying to communicate, because it’s not especially
          straightforward. To start, we should review what{" "}
          <InlineCode>useLayoutEffect</InlineCode> is actually for. Like other
          React hooks, <InlineCode>useLayoutEffect</InlineCode> provides a
          mechanism for managing side effects. In particular, as the name
          implies, layout effects are meant to be side effects that read from
          the DOM, usually for the purpose of modifying the layout of a
          component. To allow this, React will execute a component’s render
          function, commit the changes to the DOM, and then immediately run its
          layout effects <em>before</em> the browser paints those DOM updates.
          This means that something like a tooltip component can evaluate the
          position of its anchor in a layout effect, update its state to reflect
          that position, and be re-rendered with that new state, all without the
          user ever seeing the tooltip in the wrong place.
        </Paragraph>
        <Paragraph>
          Now let’s walk through what happens when we server-side render a
          component like this. Below, we have an example application that uses a
          layout effect to position a tooltip:
        </Paragraph>
        <Code
          lang="tsx"
          title="App.tsx"
        >{`import { useLayoutEffect } from "react";

export function App() {
  const [tooltipTop, setTooltipTop] = useState(0);
  const [tooltipLeft, setTooltipLeft] = useState(0);

  const anchorRef = useRef<HTMLDivElement | null>(null);

  useLayoutEffect(() => {
    if (!anchorRef.current) return;

    const rect = anchorRef.current.getBoundingClientRect();
    setTooltipTop(rect.top);
    setTooltipLeft(rect.left);
  }, []);

  return (
    <article>
      <h1>Positioned Tooltip Demo</h1>
      <div ref={anchorRef} />
      <p>A tooltip should be positioned above this paragraph.</p>
      <div style={{ position: "absolute", top, left }}>This is the tooltip</div>
    </article>
  );
}`}</Code>
        <Paragraph>
          Because we’re using a layout effect, this component will actually be
          rendered twice on mount, with both renders occurring before the DOM
          has even been painted once. The result is that the tooltip will be
          correctly positioned on the very first paint, with the user only ever
          visually seeing a DOM represented by the following HTML:
        </Paragraph>
        <Code lang="html" title="client.html">{`<article>
  <h1>Positioned Tooltip Demo</h1>
  <div></div>
  <p>A tooltip should be positioned above this paragraph.</p>
  <div style="position: absolute; top: 50px; left: 8px;"></div>
</article>`}</Code>
        <Paragraph>
          But what happens when we render this component on the server? There is
          no DOM on the server at all, so React never executes layout effects.
          Instead, the component is rendered exactly once, using the default
          values for our state:
        </Paragraph>
        <Code lang="html" title="server.html">{`<article>
  <h1>Positioned Tooltip Demo</h1>
  <div></div>
  <p>A tooltip should be positioned above this paragraph.</p>
  <!-- focus(1:1) -->
  <div style="position: absolute; top: 0; left: 0;"></div>
</article>`}</Code>
        <Paragraph>
          This means that in a server-side rendered context, until the
          client-side JavaScript bundle is loaded, parsed, and executed, the
          user will be looking at <em>the wrong UI</em>. The tooltip will simply
          be in the wrong place (at <InlineCode>0, 0</InlineCode>). It will look
          broken!
        </Paragraph>
        <Paragraph>
          This is precisely the issue that React was trying to warn us about.
          Because effect hooks don’t execute on the server at all, server-side
          rendered UIs that rely on them may appear broken until they’re
          hydrated on the client. Following the link from the warning message
          takes us to a GitHub Gist with two proposed solutions: replacing the{" "}
          <InlineCode>useLayoutEffect</InlineCode> with a{" "}
          <InlineCode>useEffect</InlineCode>, and conditionally rendering the
          component that uses <InlineCode>useLayoutEffect</InlineCode> only on
          the client. For our tooltip example, we should use the second option —
          it’s better to simply not render the tooltip at all until the
          client-side JavaScript has a chance to run and determine where it
          should be positioned.
        </Paragraph>
      </section>
      <section id="isomorphic-layout-effect">
        <Paragraph>
          <LeadIn>Not all layout effects</LeadIn> actually need to{" "}
          <em>modify</em> the layout, though. React ProseMirror, for example,
          uses layout effects internally to maintain ProseMirror’s view
          descriptor tree, which is roughly analogous to React’s virtual DOM.
          Because this requires <em>reading</em> from the DOM, but not modifying
          it, it’s actually safe to include in a server-side rendered component.
          But it’s a huge pain to fill up users’ server logs with warnings about{" "}
          <InlineCode>useLayoutEffect</InlineCode> that they can’t (and don’t
          need to) do anything about!
        </Paragraph>
        <Paragraph>
          If you’ve been around the server-side rendering block once or twice,
          you can probably see where this is going. The
          use-isomorphic-layout-effect library, or other implementations of it
          available from other popular libraries, is often the first tool that
          developers reach for when they encounter this warning. Let’s take a
          look at its implementation:
        </Paragraph>
        <Code
          lang="ts"
          title="use-isomorphic-layout-effect/src/index.ts"
        >{`import { useEffect, useLayoutEffect } from 'react'
import isClient from '#is-client'

export default isClient ? useLayoutEffect : useEffect`}</Code>
        <Paragraph>
          Very simple! The library only runs{" "}
          <InlineCode>useLayoutEffect</InlineCode> if the code is running on the
          client (in the browser, this determined via{" "}
          <InlineCode>typeof document !== &quot;undefined&quot;</InlineCode>).
          On the server, instead, it runs… <InlineCode>useEffect</InlineCode>,
          instead? That’s sort of odd. Effects never execute on the server — why
          would we bother running <InlineCode>useEffect</InlineCode> there?
        </Paragraph>
        <Paragraph>
          And it’s not just this library that’s made this somewhat odd choice of
          no-op. Here’s react-use’s implementation:
        </Paragraph>
        <Code
          lang="ts"
          title="react-use"
        >{`const useIsomorphicLayoutEffect = isBrowser ? useLayoutEffect : useEffect;`}</Code>
        <Paragraph>The Mantine design system:</Paragraph>
        <Code
          lang="ts"
          title="mantine"
        >{`export const useIsomorphicEffect = typeof document !== 'undefined' ? useLayoutEffect : useEffect;`}</Code>
        <Paragraph>React Beautiful DnD:</Paragraph>
        <Code
          lang="ts"
          title="react-beautiful-dnd"
        >{`const useIsomorphicLayoutEffect =
  typeof window !== 'undefined' &&
  typeof window.document !== 'undefined' &&
  typeof window.document.createElement !== 'undefined'
    ? useLayoutEffect
    : useEffect;`}</Code>
        <Paragraph>
          In case it’s not clear why I’m so fascinated by this choice, here’s
          React ProseMirror’s implementation:
        </Paragraph>
        <Code
          lang="ts"
          title="useClientLayoutEffect.ts"
        >{`import { useLayoutEffect } from "react";

export function useClientLayoutEffect(
  ...args: Parameters<typeof useLayoutEffect>
) {
  if (typeof document === "undefined") return;

  useLayoutEffect(...args);
}`}</Code>
        <Paragraph>
          This implementation has <em>precisely</em> the same behavior as the
          implementations above. On the client, it calls{" "}
          <InlineCode>useLayoutEffect</InlineCode>, and on the server, it does
          nothing. I didn’t name it “isomorphic”, because it’s not really
          isomorphic — at least in the sense of “Isomorphic JavaScript”, which
          describes JavaScript code that runs on both the client and the server
          — as it doesn’t run on the server at all!
        </Paragraph>
      </section>
      <section id="conclusion">
        <Paragraph>
          <LeadIn>Just to be clear</LeadIn>, this doesn’t <em>really</em>{" "}
          matter. I’m not arguing that no one should ever use
          use-isomorphic-layout-effect, or that all of these libraries need to
          change their implementations of this function to use an explicit no-op
          instead of <InlineCode>useEffect</InlineCode> on the server. I{" "}
          <em>am</em>, however, curious about where this surprisingly ubiquitous
          quirk of the React ecosystem came from. And I have a hypothesis.
        </Paragraph>
        <Paragraph>
          In February of 2019, the React team released React 16.8, the first
          stable release of React that included hooks. Two months later, React
          Redux released their v7, which included a new hooks-based integration
          between React and Redux.{" "}
          <Link href="https://github.com/reduxjs/react-redux/commit/96fec159e2afb8649321d8cd61841b9cf406ae8f#diff-9f098d970b2879a8b6675a3aea9ece3ea36c2c0d1bdfb7008f2739cda236b839R35-R281">
            And wouldn’t you know it
          </Link>
          :
        </Paragraph>
        <Code
          lang="js"
          title="connectAdvanced.js"
        >{`// React currently throws a warning when using useLayoutEffect on the server.
// To get around it, we can conditionally useEffect on the server (no-op) and
// useLayoutEffect in the browser. We need useLayoutEffect because we want
// \`connect\` to perform sync updates to a ref to save the latest props after
// a render is actually committed to the DOM.
const useIsomorphicLayoutEffect =
  typeof window !== 'undefined' ? useLayoutEffect : useEffect

...

// We need this to execute synchronously every time we re-render. However, React warns
// about useLayoutEffect in SSR, so we try to detect environment and fall back to
// just useEffect instead to avoid the warning, since neither will run anyway.
useIsomorphicLayoutEffect(() => {
`}</Code>
        <Paragraph>
          Make sure to read those comments — the React Redux team seems fully
          aware that <InlineCode>useEffect</InlineCode> is a mere no-op here.
          React Beautiful DnD’s implementation actually directly references this
          React Redux code. Other implementations likely either copied from one
          of these two popular libraries, or from{" "}
          <Link href="https://medium.com/@alexandereardon/uselayouteffect-and-ssr-192986cdcf7a">
            this Medium post from a few weeks later
          </Link>
          .
        </Paragraph>
        <Paragraph>
          From what I can tell, a very popular, well maintained library made an
          early, arbitrary implementation decision. Because copying this library
          felt like a safe bet to other library maintainers, this arbitrary
          decision became the de facto implementation for this workaround. A
          Medium post about this implementation became so widely read that it’s
          still the number one Google result for the query “useLayoutEffect ssr
          warning”, several slots above the GitHub Gist discussing the correct
          solution for most use cases.
        </Paragraph>
      </section>
      <section id="why-it-matters">
        <Paragraph>
          <LeadIn>Even though I had an explanation</LeadIn>, this kept itching
          at me. This is partly due to the description of the
          use-isomorphic-layout-effect library, which reads:
        </Paragraph>
        <BlockQuote>
          <Paragraph>
            A React helper hook for scheduling a layout effect with a fallback
            to a regular effect for environments where layout effects should not
            be used (such as server-side rendering).
          </Paragraph>
        </BlockQuote>
        <Paragraph>
          There is no mention here that <InlineCode>useEffect</InlineCode> is a
          mere no-op in those situations. It also seems to describe the problem
          space somewhat incorrectly — if a given layout effect actually should
          not be used in server-side rendering, then the component using it
          almost certainly should not be server-side rendered at all. Falling
          back to a plain effect in that situation is precisely as incorrect as
          using a layout effect — only without a warning to guide you toward the
          correct solution.
        </Paragraph>
        <Paragraph>
          react-use’s <InlineCode>useIsomorphicLayoutEffect</InlineCode> hook
          has a somewhat more accurate description:
        </Paragraph>
        <BlockQuote>
          <Paragraph>
            <InlineCode>useLayoutEffect</InlineCode> that does not show warning
            when server-side rendering, see{" "}
            <Link href="https://medium.com/@alexandereardon/uselayouteffect-and-ssr-192986cdcf7a">
              Alex Reardon’s article
            </Link>{" "}
            for more info.
          </Paragraph>
        </BlockQuote>
        <Paragraph>
          But it also lacks any detail about when it’s appropriate to use this
          hook in place of <InlineCode>useLayoutEffect</InlineCode>. And, worse,
          on the main README for react-use, the description for the hook reads:
        </Paragraph>
        <BlockQuote>
          <Paragraph>
            <InlineCode>useLayoutEffect</InlineCode> that that [sic] works on
            server.
          </Paragraph>
        </BlockQuote>
        <Paragraph>
          Which is not correct. This hook, like all other “isomorphic” layout
          effect hooks, has exactly the same behavior as{" "}
          <InlineCode>useLayoutEffect</InlineCode>, minus the warning. It does
          not work on the server!
        </Paragraph>
      </section>
      <section id="takeaways">
        <Paragraph>
          <LeadIn>
            I may be reading far too much into this very scant story
          </LeadIn>
          , but I began to see a narrative unfold the further I looked into
          this:
        </Paragraph>
        <Paragraph>
          A maintainer for a very popular open source library, in the midst of a
          big refactor, made an essentially arbitrary decision to work around a
          noisy warning that wasn’t relevant to their use case. They seem to
          have done this with full knowledge that their decision was arbitrary,
          and left a comment explaining it.
        </Paragraph>
        <Paragraph>
          Another maintainer for a similarly popular open source library also
          needed to work around the warning, which was similarly irrelevant to
          their use case. They saw this workaround and decided to copy it as-is,
          leaving only link to the original (which has since been replaced) as
          explanation.
        </Paragraph>
        <Paragraph>
          A developer, frustrated by the warning, found these libraries’
          workaround and authored a short blog post touting it as a way to quiet
          the warning. They seem to at least somewhat misunderstand the purpose
          of the warning (or maybe they fully understand it, but didn’t fully
          explain), and don’t clarify in their post that the choice of{" "}
          <InlineCode>useEffect</InlineCode> is essentially arbitrary.
        </Paragraph>
        <Paragraph>
          As more developers migrated to use React hooks, more developers ran
          into this warning and began searching for solutions. Some of them
          published the solution from Reardon’s blog post in their own
          libraries, and others found Reardon’s post and implemented his
          approach themselves.
        </Paragraph>
        <Paragraph>
          At each step in the saga, there’s less and less context. Even though
          the warning itself links to a GitHub Gist that explains the issue and
          solutions quite well, searching the language of the warning will
          retrieve Reardon’s post and other solutions before the linked Gist
          from the React team.
        </Paragraph>
        <Paragraph>
          As a result, the de facto solution to this “problem” doesn’t have
          sufficient context for users to understand how to use it effectively.
          The hugely popular{" "}
          <Link href="https://www.npmjs.com/package/react-select">
            React-Select library
          </Link>
          , for example,{" "}
          <Link href="https://github.com/JedWatson/react-select/blob/53b85346bb7defd937257b0daf9d6993ef20a59a/packages/react-select/src/components/Menu.tsx#L335-L364">
            incorrectly uses use-isomorphic-layout-effect
          </Link>{" "}
          to position and scroll a menu, when it should instead avoid rendering
          the menu on the server at all. And I’m not trying to pick on
          React-Select — it seems likely that this is almost never an actual bug
          for them, since menus are likely always collapsed during the server
          render. But that is precisely the use case that the React team had in
          mind when they added the <InlineCode>useLayoutEffect</InlineCode>{" "}
          warning!
        </Paragraph>
        <Paragraph>
          To me at least, this is a reminder of why it’s important to understand{" "}
          <em>why</em> our code does what it does. It can be tempting to sit
          back and let sleeping dogs lie after finally finding the solution to a
          confounding bug. But it’s all too easy for those incomplete
          understandings to build up and slowly shift our intuition over time,
          until we find that our mental model of our problem space doesn’t match
          reality any longer.
        </Paragraph>
        <Paragraph>
          Oh, and React ProseMirror doesn’t trigger the layout effect warning
          during server-side rendering anymore!
        </Paragraph>
      </section>
    </article>
  );
}

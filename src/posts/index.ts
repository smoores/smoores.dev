import {
  AnnouncingSmooresEpub,
  metadata as announcingSmooresEpub,
} from "./AnnouncingSmooresEpub";
import { BackToBasics, metadata as backToBasics } from "./BackToBasics";
import {
  DockerizingLegacyScoop,
  metadata as dockerizingLegacyScoop,
} from "./DockerizingLegacyScoop";
import {
  EsniPrivateByDefault,
  metadata as esniPrivateByDefault,
} from "./EsniPrivateByDefault";
import {
  NoSuchThingIsomorphicLayoutEffect,
  metadata as noSuchThingIsomorphicLayoutEffect,
} from "./NoSuchThingIsomorphicLayoutEffect";
import {
  OvercomingIoLimits,
  metadata as overcomingIoLimits,
} from "./OvercomingIoLimits";
import {
  PhoneticMatching,
  metadata as phoneticMatching,
} from "./PhoneticMatching";
import {
  SmtpConversation,
  metadata as smtpConversation,
} from "./SmtpConversation";
import {
  SomeoneMustHave,
  metadata as someoneMustHave,
} from "./SomeoneMustHave";
import {
  StorytellerIsOnPikaPods,
  metadata as storytellerIsOnPikaPods,
} from "./StorytellerIsOnPikaPods";
import {
  WhatIsSmooresDev,
  metadata as whatIsSmooresDev,
} from "./WhatIsSmooresDev";
import {
  WhyIRebuiltProseMirrorView,
  metadata as whyIRebuiltProseMirrorView,
} from "./WhyIRebuiltProseMirrorView";

export const posts = [
  {
    Component: NoSuchThingIsomorphicLayoutEffect,
    metadata: noSuchThingIsomorphicLayoutEffect,
  },
  {
    Component: WhyIRebuiltProseMirrorView,
    metadata: whyIRebuiltProseMirrorView,
  },
  { Component: SomeoneMustHave, metadata: someoneMustHave },
  { Component: StorytellerIsOnPikaPods, metadata: storytellerIsOnPikaPods },
  { Component: AnnouncingSmooresEpub, metadata: announcingSmooresEpub },
  { Component: WhatIsSmooresDev, metadata: whatIsSmooresDev },
  { Component: OvercomingIoLimits, metadata: overcomingIoLimits },
  { Component: PhoneticMatching, metadata: phoneticMatching },
  {
    Component: BackToBasics,
    metadata: backToBasics,
  },
  {
    Component: EsniPrivateByDefault,
    metadata: esniPrivateByDefault,
  },
  {
    Component: DockerizingLegacyScoop,
    metadata: dockerizingLegacyScoop,
  },
  {
    Component: SmtpConversation,
    metadata: smtpConversation,
  },
];

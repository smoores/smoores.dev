import { DateLine } from "@/components/DateLine";
import { H3 } from "@/components/H3";
import { LeadIn } from "@/components/LeadIn";
import { Link } from "@/components/Link";
import { Paragraph } from "@/components/Paragraph";
import { UnderlinedHeading } from "@/components/UnderlinedHeading";
import { Metadata } from "@/metadata";

export const metadata: Metadata = {
  title: "Storyteller is on PikaPods!",
  slug: "storyteller_is_on_pikapods",
  date: "Dec. 18, 2024",
  description:
    "PikaPods is a paid service for hosting open source apps like Storyteller. You can decide what resources are necessary to meet your needs, scale your pods up or down as needed, and deploy new apps with the click of a button. And now you can use PikaPods to easily spin up your own Storyteller instance!",
};

export function StorytellerIsOnPikaPods() {
  return (
    <article>
      <UnderlinedHeading>{metadata.title}</UnderlinedHeading>
      <DateLine date={metadata.date} />
      <section id="intro">
        <Paragraph className="text-sm">
          Just looking for a link?{" "}
          <Link href="https://www.pikapods.com/pods?run=storyteller">
            Click here to run your own Storyteller pod on PikaPods!
          </Link>
        </Paragraph>
      </section>
      <section id="what-is-storyteller">
        <H3>What is Storyteller?</H3>
        <Paragraph>
          <Link href="https://smoores.gitlab.io/storyteller">Storyteller</Link>{" "}
          is a self-hosted, open source platform for automatically aligning
          audiobooks and ebooks, and reading ebooks with aligned audio. That’s a
          very dense sentence, so let’s break it down a bit:
        </Paragraph>
        <Paragraph>
          <LeadIn>self-hosted</LeadIn> means that Storyteller runs on{" "}
          <em>your</em> computer, not mine. When you add a book to Storyteller,
          it’s saved to your hard drive, and only you have control over what
          happens with your Storyteller instance’s data.
        </Paragraph>
        <Paragraph>
          <LeadIn>open source</LeadIn> means that that anyone can read (and
          reuse) the code that makes up Storyteller. It’s publicly available on
          GitLab, in the{" "}
          <Link href="https://gitlab.com/smoores/storyteller">
            smoores/storyteller
          </Link>{" "}
          repo.
        </Paragraph>
        <Paragraph>
          <LeadIn>aligning audibooks and ebooks</LeadIn> is Storyteller’s
          primary purpose. You can provide Storyteller with the audiobook and
          ebook for the same book, and it will produce a new, aligned ebook,
          with the audio baked in.
        </Paragraph>
        <Paragraph>
          <LeadIn>ebooks with aligned audio</LeadIn> — also called “guided
          narration” or “media overlays” — allows you to switch back and forth
          between reading and listening to a book without ever losing your
          place. You can even read and listen at the same time, and the
          Storyteller reader app (or any other app compatible with EPUB Media
          Overlays!) will highlight the current sentence as it reads.
        </Paragraph>
        <Paragraph>
          I’m a firm believer in the value of free, open source, software. This
          is why Storyteller is licensed with the MIT license, meaning anyone is
          free to use the code that makes up Storyteller however they please,
          including in other projects. And I think that everyone should be give
          the opportunity to <em>truly</em> own their media, even when it’s
          digital, which is why Storyteller is designed to be self-hosted,
          rather than a service that I run myself and others pay to use.
        </Paragraph>
        <Paragraph>
          However, not everyone is interested in learning how to run software
          like Storyteller themselves. Storyteller isn’t just an app that you
          can install on your phone or laptop — it’s a full stack platform, with
          a backend server that needs to be running at all times. And
          Storyteller has proven to have an appeal beyond the niche community of
          self-hosting enthusiasts — reading books with guided narration is
          really valuable!
        </Paragraph>
        <Paragraph>
          So, with an eye toward making Storyteller accessible to more folks,
          and without compromising the principles of self-hosting and digital
          media ownership, I’m genuinely ecstatic to announce that Storyteller
          is now available on PikaPods!
        </Paragraph>
      </section>
      <section id="what-is-pika-pods">
        <H3>What is PikaPods?</H3>
        <Paragraph>
          <Link href="https://www.pikapods.com/">PikaPods</Link> is a paid
          service for hosting open source apps like Storyteller. You can decide
          what resources are necessary to meet your needs, scale your pods up or
          down as needed, and deploy new apps with the click of a button.
        </Paragraph>
        <Paragraph>
          PikaPods also shares profits with open source developers, so by using
          Storyteller on PikaPods, you’re also funding future Storyteller
          development. And PikaPods has dozens of other open source apps
          available, many of which I use myself!
        </Paragraph>
        <Paragraph>
          After a potential Storyteller user{" "}
          <Link href="https://gitlab.com/smoores/storyteller/-/issues/164">
            asked for a way for less technically inclined folks to access
            Storyteller
          </Link>
          , I reached out to the folks at PikaPods. They were kind and
          responsive, happily answered all of my questions, and after just two
          weeks had Storyteller up and running.
        </Paragraph>
      </section>
      <section id="what-else">
        <H3>What else do I need to know?</H3>
        <Paragraph>
          Storyteller is a fairly resource intensive application. Audiobooks are
          usually <em>very</em> long, so transcoding them, transcribing them,
          and running Storyteller’s forced alignment algorithm uses a lot of
          memory, a lot of CPU, and a lot of time. This means that Storyteller
          has larger default resource requirements on PikaPods than many other
          apps do. It also defaults to re-encoding audio with the OPUS audio
          codec, which helps with memory usage, as well as reducing the eventual
          file size for the aligned books. Even so, you may find that you need
          to bump up the memory allocations for your pod if you regularly align
          very long books.
        </Paragraph>
        <Paragraph>
          PikaPods also doesn’t have any GPUs available, so transcription has to
          run on the CPU, which can be slow. If you choose, you can configure a
          hosted transcription service — like OpenAI Cloud, AWS Transcribe, or
          Google Cloud Speech-to-Text — in the Storyteller settings. These are
          paid services, but they tend to be much, much faster than running
          transcription on the CPU.
        </Paragraph>
        <Paragraph>
          As always, if you need help with Storyteller, reach out on{" "}
          <Link href="https://matrix.to/#/#smoores_storyteller:gitter.im">
            Matrix
          </Link>
          , <Link href="https://discord.gg/KhSvFqcrza">Discord</Link>, or{" "}
          <Link href="https://storyteller.discourse.group/">Discourse</Link>.
        </Paragraph>
      </section>
    </article>
  );
}

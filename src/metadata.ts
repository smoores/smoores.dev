export interface Metadata {
  slug: string;
  date: string;
  updated?: string;
  title: string;
  description: string;
}

import { HTMLProps } from "react";
import cx from "classnames";

export function InlineCode(props: HTMLProps<HTMLElement>) {
  return (
    <code
      {...props}
      className={cx("rounded bg-gray-300 p-0.5", props.className)}
    />
  );
}

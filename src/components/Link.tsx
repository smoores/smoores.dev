import cx from "classnames";
import { AnchorHTMLAttributes } from "react";

export function Link(props: AnchorHTMLAttributes<HTMLAnchorElement>) {
  return (
    <a
      {...props}
      className={cx(
        props.className,
        "font-bold text-smoores-orange-shade2 visited:bg-smoores-orange-shade1 hover:underline",
      )}
    />
  );
}

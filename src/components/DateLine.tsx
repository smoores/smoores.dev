interface Props {
  date: string;
}

export function DateLine({ date }: Props) {
  return <p className="mb-3 text-sm">{date}</p>;
}

import { HTMLAttributes } from "react";
import cx from "classnames";
import { H2 } from "./H2";

export function UnderlinedHeading({
  children,
  ...props
}: HTMLAttributes<HTMLHeadingElement>) {
  return (
    <div
      {...props}
      className={cx(props.className, "text-smoores-orange underline")}
    >
      <H2 className="text-primary">{children}</H2>
    </div>
  );
}

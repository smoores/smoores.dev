import { HTMLAttributes } from "react";
import cx from "classnames";

export function H2(props: HTMLAttributes<HTMLHeadingElement>) {
  return (
    <h2 {...props} className={cx(props.className, "my-6 text-2xl font-bold")} />
  );
}

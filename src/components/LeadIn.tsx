import { HTMLAttributes } from "react";
import cx from "classnames";

export function LeadIn({
  children,
  ...props
}: HTMLAttributes<HTMLSpanElement>) {
  return (
    <span
      {...props}
      className={cx("text-smoores-orange underline", props.className)}
    >
      <span className="text-lg font-bold text-primary">{children}</span>
    </span>
  );
}

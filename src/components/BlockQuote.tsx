import { HTMLAttributes } from "react";
import cx from "classnames";

export function BlockQuote(props: HTMLAttributes<HTMLElement>) {
  return (
    <blockquote {...props} className={cx(props.className, "mx-10 my-5")} />
  );
}

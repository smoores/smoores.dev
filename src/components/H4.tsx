import { HTMLAttributes } from "react";
import cx from "classnames";

export function H4(props: HTMLAttributes<HTMLHeadingElement>) {
  return (
    <h4 {...props} className={cx(props.className, "my-5 text-lg font-bold")} />
  );
}

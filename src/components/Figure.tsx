import { HTMLAttributes } from "react";
import cx from "classnames";

export function Figure(props: HTMLAttributes<HTMLElement>) {
  return (
    <figure
      {...props}
      className={cx(props.className, "m-0 flex flex-col items-center md:m-8")}
    />
  );
}

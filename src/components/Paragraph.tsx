import { HTMLAttributes } from "react";
import cx from "classnames";

export function Paragraph(props: HTMLAttributes<HTMLParagraphElement>) {
  return (
    <p {...props} className={cx(props.className, "my-4 leading-[1.6rem]")} />
  );
}

import { HTMLAttributes } from "react";
import cx from "classnames";

export function FigCaption(props: HTMLAttributes<HTMLElement>) {
  return <figcaption {...props} className={cx(props.className, "text-sm")} />;
}

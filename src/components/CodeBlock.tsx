import { ReactNode } from "react";

interface Props {
  children: ReactNode;
}

export function CodeBlock({ children }: Props) {
  return (
    <pre className="overflow-x-auto">
      <code>{children}</code>
    </pre>
  );
}

import { HTMLAttributes } from "react";
import cx from "classnames";

export function Subtitle(props: HTMLAttributes<HTMLParagraphElement>) {
  return <p {...props} className={cx(props.className, "mb-4 mt-0 text-sm")} />;
}

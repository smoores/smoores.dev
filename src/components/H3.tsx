import { HTMLAttributes } from "react";
import cx from "classnames";

export function H3(props: HTMLAttributes<HTMLHeadingElement>) {
  return (
    <h3 {...props} className={cx(props.className, "my-5 text-xl font-bold")} />
  );
}

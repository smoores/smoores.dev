// import { renderToStaticMarkup } from "react-dom/server";
import { posts } from "@/posts";
import escapeHTML from "escape-html";
import { NextResponse } from "next/server";
import { ReadableStream } from "node:stream/web";
import { createElement, ReactNode } from "react";
import type {
  ReactDOMServerReadableStream,
  RenderToReadableStreamOptions,
} from "react-dom/server";

export const dynamic = "force-static";

export async function GET() {
  // This has to be dynamically imported to work around this:
  // https://github.com/vercel/next.js/issues/43810
  const renderToReadableStream: (
    node: ReactNode,
    options?: RenderToReadableStreamOptions,
  ) => Promise<ReactDOMServerReadableStream> =
    // @ts-expect-error We have to import from server.browser, because
    // otherwise we get the node.js file, which doesn't expose renderToReadableStream
    // (even though we have ReadableStreams available)
    (await import("react-dom/server.browser")).renderToReadableStream;

  const author = "smoores (Shane Friedman)";

  const postEntries = [];
  for (const post of posts) {
    const markupBytesStream = await renderToReadableStream(
      createElement(post.Component, { serverOnly: true }),
    );

    await markupBytesStream.allReady;

    const markupStream = markupBytesStream.pipeThrough(new TextDecoderStream());

    let markup = "";
    for await (const chunk of markupStream as ReadableStream<string>) {
      markup += chunk;
    }
    const path = `/post/${post.metadata.slug}/`;
    const published = new Date(post.metadata.date).toISOString();
    const updated =
      (post.metadata.updated &&
        new Date(post.metadata.updated).toISOString()) ??
      published;

    postEntries.push({
      title: post.metadata.title,
      updated,
      published,
      path,
      author,
      summary: post.metadata.description,
      content: escapeHTML(markup),
    });
  }

  postEntries.sort(
    (a, b) => new Date(a.published).valueOf() - new Date(b.published).valueOf(),
  );

  const latestUpdated = postEntries.reduce((a, b) => {
    if (new Date(a.updated).valueOf() > new Date(b.updated).valueOf()) {
      return a;
    }
    return b;
  });

  const response = `<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title type="text">smoores.dev - Recent Posts</title>
  <author>
    <name>${author}</name>
    <uri>https://resume.smoores.dev</uri>
  </author>
  <icon>https://smoores.dev/favicon.ico</icon>
  <id>https://smoores.dev/recent.atom</id>
  <updated>${latestUpdated.updated}</updated>
  <link href="https://smoores.dev/" />
  <link href="https://smoores.dev/recent.atom" rel="self" />
${postEntries
  .map(
    (post) => `  <entry>
    <title type="text">${post.title}</title>
    <id>https://smoores.dev${post.path}</id>
    <updated>${post.updated}</updated>
    <published>${post.published}</published>
    <link href="https://smoores.dev${post.path}" />
    <author>
      <name>${post.author}</name>
      <uri>https://resume.smoores.dev</uri>
    </author>
    <summary type="text">${post.summary}</summary>
    <content type="html" xml:lang="en" xml:base="http://smoores.dev${post.path}">
      ${post.content}
    </content>
  </entry>
`,
  )
  .join("\n")}
</feed>
`;

  return new NextResponse(response, {
    headers: {
      "Content-Type": "application/atom+xml",
    },
  });
}

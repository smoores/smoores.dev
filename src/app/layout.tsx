import type { Metadata } from "next";
import { Vollkorn } from "next/font/google";
import "./globals.css";
import Script from "next/script";

const vollkorn = Vollkorn({
  subsets: ["latin"],
  variable: "--font-vollkorn",
  weight: "variable",
});

export const metadata: Metadata = {
  title: "smoores.dev",
  description:
    "I'm Shane. I build stuff that lives on the internet, and sometimes I write about it, too. If you want to learn a little bit more about me, you can check out my résumé at https://resume.smoores.dev.",
  alternates: {
    types: {
      "application/atom+xml": [
        {
          url: "https://smoores.dev/recent.atom",
          title: "smoores.dev",
        },
      ],
    },
  },
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className="h-full">
      <body
        suppressHydrationWarning
        className={`${vollkorn.variable} m-0 flex h-full flex-col justify-between text-foreground antialiased`}
      >
        <div className="mx-auto my-0 w-[calc(100%-2rem)] md:w-page">
          {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
          <a href="/" className="block">
            <header>
              <h1 className="mb-0 mt-6 text-4xl font-bold text-primary">
                smoores.dev
              </h1>
              <p className="mt-2 text-base text-primary">
                Building stuff on the Internet.
              </p>
            </header>
          </a>
          <main className="mt-12 w-full md:w-page">{children}</main>
        </div>
        <footer className="mt-8 p-8 text-sm">
          <a href="/about_fathom" className="hover:underline">
            This website does not track you! Click here for more information
            about the analytics I gather.
          </a>
        </footer>
        <Script src="/tracking.js"></Script>
      </body>
    </html>
  );
}

import { DateLine } from "@/components/DateLine";
import { H2 } from "@/components/H2";
import { Link } from "@/components/Link";
import { Paragraph } from "@/components/Paragraph";
import { UnderlinedHeading } from "@/components/UnderlinedHeading";
import { posts } from "@/posts";
import { H3 } from "@/components/H3";

export default function Home() {
  return (
    <>
      <section className="text-primary">
        <H2>Welcome!</H2>
        <p>
          I’m Shane. I build stuff that lives on the internet, and sometimes I
          write about it, too. If you want to learn a little bit more about me,
          you can check out my résumé at{" "}
          <Link href="https://resume.smoores.dev">resume.smoores.dev</Link>.
        </p>
      </section>
      <UnderlinedHeading>Recent Posts</UnderlinedHeading>
      {posts.map(({ metadata }) => (
        <a
          key={metadata.slug}
          href={`/post/${metadata.slug}/`}
          className="hover:underline"
        >
          <article>
            <H3>{metadata.title}</H3>
            <DateLine date={metadata.date} />
            <Paragraph className="my-4">{metadata.description}</Paragraph>
          </article>
        </a>
      ))}
      <UnderlinedHeading>Recent Talks</UnderlinedHeading>
      <Paragraph>
        Slides for talks that I’ve given recently. These are usually made with{" "}
        <Link href="https://revealjs.com/" rel="noopener noreferer">
          reveal.js
        </Link>{" "}
        and hosted on my personal web server.
      </Paragraph>
      <a className="hover:underline" href="https://ink.talks.smoores.dev">
        <article>
          <H3>A Tale of 100 Blue Buttons</H3>
          <Paragraph>
            A brief intro to The New York Times’ content management system, and
            why we needed to build Ink, a design system for that content
            management system.
          </Paragraph>
        </article>
      </a>
      <a className="hover:underline" href="https://promises.talks.smoores.dev">
        <article>
          <H3>Monads and You: How to think about Promises</H3>
          <Paragraph>
            A look at how the functional programming notions of functors and
            monads can help provide us with helpful mental models for thinking
            about Promises in Javascript. It’s not as scary as it sounds,
            really!
          </Paragraph>
        </article>
      </a>
      <a
        className="hover:underline"
        href="https://yarn-berry.talks.smoores.dev"
      >
        <article>
          <H3>Yarn Berry</H3>
          <Paragraph>
            A talk about how and why to migrate to Yarn Berry.
          </Paragraph>
        </article>
      </a>
      <a
        className="hover:underline"
        href="https://newsguildny.github.io/times-tech-guild-sl-2022/"
      >
        <article>
          <H3>Unionizing NYTimes Tech Workers—with bubbles and red paint</H3>
          <Paragraph>
            A talk given by myself and my co-worker and fellow union organizer
            Vicki Crosson about how the New York Times Tech Guild built software
            to assist in their organizing process.
          </Paragraph>
        </article>
      </a>
    </>
  );
}

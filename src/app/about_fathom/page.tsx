import { Link } from "@/components/Link";
import { UnderlinedHeading } from "@/components/UnderlinedHeading";

export default function AboutFathomPage() {
  return (
    <article>
      <UnderlinedHeading>
        <h2>This Blog’s Analytics Gathering</h2>
      </UnderlinedHeading>
      <p>
        This blog uses <Link href="https://usefathom.com">Fathom</Link> for site
        analytics. It’s self-hosted, which means the data doesn’t leave the
        server that the website is hosted on. It’s also privacy-centric; I don’t
        know anything other than the number of users that visited the blog on a
        given hour of the day. Here’s what the dashboard looks like from my
        personal blog (impressive, right?):
      </p>
      {/* eslint-disable-next-line @next/next/no-img-element */}
      <img
        className="my-8"
        src="/images/fathom_dash.png"
        alt="A screenshot of the Fathom analytics dashboard, showing 4 unique visitors, 11 pageviews, a 25% bounce rate, and a basic breakdown of page views per page"
      />
      <p>
        That’s it, that’s all I get. No IP addresses, location data, or device
        information.
        <a href="https://usefathom.com/data/">See here</a> for a discussion of
        Fathom’s data policy, specifically as it relates to the EU’s General
        Data Protection Regulation (GDPR). Plus, if you have the “Do Not Track”
        setting turned on in your browser, Fathom doesn’t record{" "}
        <em>anything</em>. Seriously, look at the “trackPageview” function in{" "}
        <a target="_blank" href="https://fathom.shanemoore.me/tracker.js">
          fathom.shanemoore.me/tracker.js
        </a>
        .
      </p>
    </article>
  );
}

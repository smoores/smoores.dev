import { posts } from "@/posts";
import { Metadata } from "next";
import { notFound } from "next/navigation";

interface Props {
  params: Promise<{
    slug: string;
  }>;
}

export default async function PostPage({ params }: Props) {
  const { slug } = await params;
  const post = posts.find(({ metadata }) => metadata.slug === slug);
  if (!post) {
    return notFound();
  }

  return <post.Component />;
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const { slug } = await params;
  const post = posts.find(({ metadata }) => metadata.slug === slug);
  if (!post) {
    return notFound();
  }
  return {
    title: `smoores.dev - ${post.metadata.title}`,
    description: post.metadata.description,
    openGraph: {
      type: "article",
    },
  };
}

export async function generateStaticParams() {
  return posts.map((post) => ({
    slug: post.metadata.slug,
  }));
}
